# migu3d-decrypt

### 暂不开源，采用Golang编写

#### 介绍

咪咕3D音乐解密
本工具完全免费！

最新版本 v1.0.2

现在已经支持无歌曲id和filekey解密了(beta)

#### 使用说明
options:

````
A migu 3d audio decrypt cli.

Usage:
  migu3d-decrypt [command]

Available Commands:
  completion  Generate the autocompletion script for the specified shell
  decrypt     Decrypt local file, only support [mg3d|m4a|wav] format
  help        Help about any command
  serve       Start a http server
  update      Check for update

Flags:
  -h, --help      help for migu3d-decrypt
  -t, --toggle    Help message for toggle
  -v, --version   version for migu3d-decrypt

Use "migu3d-decrypt [command] --help" for more information about a command.
````

### 启动http服务方法

`migu3d-decrypt serve`

然后复制地址到设置里面
类似`http://127.0.0.1:10085`

###解密歌曲
song id 和 file key 二选一，至少要输入一个 优先使用file key